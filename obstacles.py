from gamelib import SpriteEntity
from smoke import Smoke
from bullet import Bullet
from random import choice, random

OBS_IMAGE_FILES = [
    "resources/obstacles/barrelGreen_side_damaged.png",
    "resources/obstacles/barrelGreen_side.png",
    "resources/obstacles/barrelGrey_side.png",
    "resources/obstacles/barrelRed_side.png",
    "resources/obstacles/barrelGreen_up.png",
    "resources/obstacles/barrelGrey_up.png",
    "resources/obstacles/barrelRed_up.png",
    "resources/env/treeLarge.png",
    "resources/env/treeSmall.png",
]

class Obstacle(SpriteEntity):

    def __init__(self, game) -> None:
        super().__init__(game, choice(OBS_IMAGE_FILES), 1)
        self.__damaged = False

    def update(self, dt: float):
        if self.__damaged:
            if random() * dt > 0.0001:
                Smoke(self.game, self)
        
        for obstacle in self.game.get_entities_of_type(Bullet): 
            if self.collides_with(obstacle):
                obstacle.remove()
                if self.__damaged:
                    self.remove()
                    return
                else:
                    self.__damaged = True
                break


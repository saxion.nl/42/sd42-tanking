from __future__ import annotations
import pyglet
from abc import ABC, abstractmethod


def _create_proxy(attr: str):
    """Creates a method that relays all gets and sets to an attribute with 
    the specified name on `self._drawable`. It uses the `property` and `setter`
    function decorators.

    Args:
        attr (str): The name of the attribute on `self._drawable` to set/get.
    """
    def set_value(self, value):
        setattr(self._drawable, attr, value)
    def get_value(self):
        return getattr(self._drawable, attr)
    return property(get_value).setter(set_value)


class Entity():
    def __init__(self, game, drawable, **kwargs):
        self.__game = game
        self._drawable = drawable
        # Add us to the (semi-private) list of entities.
        game._entities.add(self)
        # Try to apply any additional keyword arguments as attributes.
        for name,value in kwargs.items():
            if not hasattr(self, name):
                raise ValueError(f"'{self.__class__.__name__}' has no '{name}' attribute")
            setattr(self, name, value)

    @property
    def game(self):
        return self.__game

    def remove(self):
        if self.__game:
            self.__game._entities.remove(self)
            self.__game = None
            self._drawable.batch = None
            self._drawable.delete()

    def update(self, dt: float):
        pass

    def collides_with(self, other: Entity, overlap: int = 0):
        return self.right > other.left+overlap and self.left+overlap < other.right and \
               self.top > other.bottom+overlap and self.bottom+overlap < other.top

    x: float = _create_proxy('x')
    """Horizontal position of the center of the entity relative to the left border of the window."""

    y: float = _create_proxy('y')
    """Vertical position of the center of the entity relative to the bottom of the window."""

    opacity: int = _create_proxy("opacity")
    """Blend opacity. An opacity of 255 means full opaque (solid), 128 means
    semi-transparent and 0 means fully invisible. Defaults to 255."""

    width: int = _create_proxy('width')

    height: int = _create_proxy('height')

    @property
    def top(self) -> float:
        """Vertical position of the top of the entity relative to the bottom of the window."""
        return self.y + self.height/2

    @top.setter
    def top(self, value:float):
        # There's no need to document setters, as they're conceptually the same thing as their getter.
        self.y = value - self.height/2

    @property
    def bottom(self) -> float:
        return self.y - self.height/2

    @bottom.setter
    def bottom(self, value:float):
        self.y = value + self.height/2

    @property
    def left(self) -> float:
        return self.x - self.width/2

    @left.setter
    def left(self, value:float):
        self.x = value + self.width/2

    @property
    def right(self) -> float:
        return self.x + self.width/2

    @right.setter
    def right(self, value:float):
        self.x = value - self.width/2



class SpriteEntity(Entity):
    """A game object composed of an image that's displayed somewhere on the screen,
    optionally scaled, rotated and semi-transparent,"""

    __images_by_name: dict[str,pyglet.image.AbstractImage] = {}

    def __init__(self, game: Game, image_file: str, layer: int = 0, **kwargs):
        """Create a new `SpriteEntity`, loading the image from the given file name, and 
        register it with the `Game`.

        Args:
            game (Game): The `Game` that the created entity should be registered with.
            image_file (str): The name of the image file to use.
            layer (int, optional): The layer on which to draw this entity. Layers with higher
                numbers are drawn in front of layers with lower numbers. Defaults to 0.
        """
        drawable = pyglet.sprite.Sprite(self.__load_image(image_file), batch=game._get_batch(), group=game._get_group(layer))
        super().__init__(game, drawable, **kwargs)
        self.__image_file = image_file

    @classmethod
    def __load_image(cls, file_name: str):
        image = cls.__images_by_name.get(file_name)
        if not image:
            image = cls.__images_by_name[file_name] = pyglet.image.load(file_name)
            image.anchor_x = image.width // 2
            image.anchor_y = image.height // 2
        return image

    @property
    def image_file(self) -> str:
        return self.__image_file

    @image_file.setter
    def image_file(self, file_name: str):
        self.__image_file = file_name
        self._drawable.image = self.__load_image(file_name)

    rotation: float = _create_proxy("rotation")
    
    scale: float  = _create_proxy("scale")



class ColorEntity(Entity):
    @property
    def color(self) -> tuple[int]:
        return self._drawable.color

    @color.setter
    def color(self, value: tuple[int,...]|str):
        if type(value)==str:
            # A CSS-style color string like `#ef6b00` or `#ef6b0080`. Convert to (r,g,b,a,) tuple.
            if value[0] == "#": value = value[1:] # Strip the hash
            if len(value) == 6: value += 'FF' # Add the opacity
            assert len(value) == 8
            value = tuple(int(value[i:i+2],16) for i in range(0,8,2))
        self._drawable.color = value



class LabelEntity(ColorEntity):
    def __init__(self, game: Game, text: str = "", layer: int = 0, **kwargs):
        drawable = pyglet.text.Label(text, batch=game._get_batch(), group=game._get_group(layer), anchor_x='center', anchor_y='center')
        super().__init__(game, drawable, **kwargs)

    # We're overriding width and height here, as their values have a different meaning in pyglet labels. 
    # The things we're after are called `content_width` and `content_height`.

    @property
    def width(self):
        return self._drawable.content_width

    @property
    def height(self):
        return self._drawable.content_height

    font_name: str = _create_proxy("font_name")

    font_size: int = _create_proxy("font_size")

    text: str = _create_proxy("text")

    bold: bool = _create_proxy("bold")

    italic: bool = _create_proxy("italic")



class RectEntity(ColorEntity):
    def __init__(self, game: Game, layer: int = 0, **kwargs):
        drawable = pyglet.shapes.Rectangle(batch=game._get_batch(), group=game._get_group(layer), x=10, y=10, width=10, height=10)
        drawable.anchor_x = 5
        drawable.anchor_y = 5
        super().__init__(game, drawable, **kwargs)

    @property
    def width(self) -> float:
        return self._drawable.width
    
    @width.setter
    def width(self, value):
        self._drawable.width = value
        self._drawable.anchor_x = value/2

    @property
    def height(self) -> float:
        return self._drawable.height
    
    @height.setter
    def height(self, value):
        self._drawable.height = value
        self._drawable.anchor_y = value/2



class Game():
    __sounds: dict[str,pyglet.media.StaticSource] = {}
    __layer_groups: dict[int,pyglet.graphics.Group] = {}

    def __init__(self, width: int, height: int, **kwargs):
        self._entities: set[Entity] = set()
        self.__batch = pyglet.graphics.Batch()

        self.keys_down: set[str] = set()
        """A set of textual representations for each of the keys that is currently being held down."""
        self.keys_pressed: list[str] = []
        """An ordered list of all keys that have been pressed since the last *update*."""

        # Create a pyglet window.
        self.__window = pyglet.window.Window(width, height, **kwargs)

        # Game-specific initialization.
        self.start()

        # Setup event handling methods.
        self.__window.on_key_press = self.__on_key_press
        self.__window.on_key_release = self.__on_key_release
        self.__window.on_draw = self.__on_draw
        pyglet.clock.schedule_interval(self.__update, 1/60) # max 60 fps

    @abstractmethod
    def start(self):
        pass

    def update(self, dt):
        pass

    @property
    def width(self):
        return self.__window.width

    @property
    def height(self):
        return self.__window.height

    def clear_entities(self):
        for entity in set(self._entities):
            entity.remove()

    def __update(self, dt: float):
        self.update(dt)
        for entity in set(self._entities):
            if entity.game:
                entity.update(dt)
        self.keys_pressed.clear()

    def get_entities_of_type(self, EntityType: type):
        return [entity for entity in self._entities if isinstance(entity, EntityType)]

    def play_sound(self, file_name: str):
        if file_name not in self.__sounds:
            self.__sounds[file_name] = pyglet.media.load(file_name, streaming=False)
        self.__sounds[file_name].play()

    @staticmethod
    def run():
        pyglet.app.run()

    def _get_batch(self):
        return self.__batch

    def _get_group(self, layer:int) -> pyglet.group.Group:
        if layer not in self.__layer_groups:
            self.__layer_groups[layer] = pyglet.graphics.Group(layer)
        return self.__layer_groups[layer]

    def __on_key_press(self, symbol: int, modifiers: int):
        name = pyglet.window.key.symbol_string(symbol)
        self.keys_down.add(name)
        self.keys_pressed.append(name)

    def __on_key_release(self, symbol: int, modifiers: int):
        name = pyglet.window.key.symbol_string(symbol)
        try:
            self.keys_down.remove(name)
        except KeyError:
            pass

    def __on_draw(self):
        self.__window.clear()
        self.__batch.draw()

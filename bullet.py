import math
from gamelib import SpriteEntity

class Bullet(SpriteEntity):
    def __init__(self, game, tank):
        super().__init__(game, f"resources/bullets/bullet{tank.color}.png", 4)
        self.x = tank.x
        self.y = tank.y
        self.rotation = tank.rotation
        self.__origin_tank = tank

    def update(self, dt):
        distance_y = dt * 300
        radians = self.rotation / 360 * 2 * math.pi
        self.x += math.sin(radians)*distance_y
        self.y += math.cos(radians)*distance_y

        for tank in self.game.get_entities_of_type(self.__origin_tank.__class__):
            if tank != self.__origin_tank and tank.collides_with(self):
                tank.die_a_little()
                self.remove()
            


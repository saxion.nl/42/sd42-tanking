from gamelib import SpriteEntity
import random

class Smoke(SpriteEntity):
    def __init__(self, game, tank):
        rnd = random.randrange(6)
        super().__init__(game, f'resources/smoke/smokeGrey{rnd}.png', 5)

        self.x = tank.x
        self.y = tank.y
        self.opacity = 18

    def update(self, dt):
        self.opacity -= dt * 20
        self.x += dt * 50
        self.y += dt * 20
        self.scale *= (1+dt)
        if self.opacity < 1:
            self.remove()

import math
import random
from gamelib import SpriteEntity
from bullet import Bullet
from obstacles import Obstacle
from smoke import Smoke

class Tank(SpriteEntity):
    def __init__(self, game, color, position=(10,10), keys=['UP','RIGHT','DOWN','LEFT', 'RSHIFT']):
        self.__color = color
        super().__init__(game, f'resources/tanks/tank{self.__color}.png', 2)

        #Add keybinding
        self.__keys = keys

        self.barrel = SpriteEntity(game, f'resources/tanks/barrel{self.__color}.png', 3)

        self.x = position[0]
        self.y = position[1]
        self.rotation = 0

        self.barrel.x = position[0]
        self.barrel.y = position[1]

        self.__reload_time_left = 0
        self.__smoking = False


    @property
    def color(self):
        return self.__color


    def die_a_little(self):
        self.__smoking = True


    def update(self, dt):
        # print(self.game.keys_down)
        if self.__keys[1] in self.game.keys_down:
            self.rotation += dt*100
        if self.__keys[3] in self.game.keys_down:
            self.rotation -= dt*100

        old_x = self.x
        old_y = self.y
        speed = 0

        if self.__keys[0] in self.game.keys_down:
            speed += 100
        if self.__keys[2] in self.game.keys_down:
            speed -= 100

        radians = self.rotation / 360 * 2 * math.pi
        self.x += math.sin(radians)*(dt * speed)
        self.y += math.cos(radians)*(dt * speed)

        for obstacle in self.game.get_entities_of_type(Obstacle) + self.game.get_entities_of_type(Tank): 
            if self.collides_with(obstacle, 10) and obstacle != self:
                self.x = old_x
                self.y = old_y

        self.barrel.rotation = self.rotation
        self.barrel.x = self.x
        self.barrel.y = self.y
        
        self.__reload_time_left -= dt
        if self.__keys[4] in self.game.keys_down:
            if self.__reload_time_left <= 0:
                Bullet(self.game, self)
                self.__reload_time_left = 1.5 # seconds

        if self.__smoking:
            if random.random() * dt > 0.0001:
                Smoke(self.game, self)


from tank import Tank
from gamelib import *
from obstacles import Obstacle
from random import randint
from background import draw_background

COLS = 8
ROWS = 6

class TanksGame(Game):
    def __init__(self):
        super().__init__(COLS * 128, ROWS * 128)
        
        # Add your initial entities here
        Tank(self, 'Red', (60,60), ['W', 'D', 'S', 'A','LSHIFT'])
        Tank(self, 'Blue', (self.width - 60, self.height - 60))

        # Create a background
        draw_background(self)
   
        # Create some obstacles
        for r in range(ROWS):
            for c in range(COLS):
                # Do we want an obstacle?
                if randint(0, 3) == 0:
                    o = Obstacle(self)
                    o.x = (c * 128) + randint(0, 64) + 32
                    o.y = (r * 128) + randint(0, 64) + 32



if __name__ == "__main__":
    TanksGame().run()

import gamelib, random

ENV = [
    'resources/env/dirt.png',
    'resources/env/grass.png',
    'resources/env/sand.png'
]

def draw_background(game : gamelib.Game):
    for x in range(64, game.width, 128):
        for y in range(64, game.height, 128):
            file = random.choice(ENV)
            shape = gamelib.SpriteEntity(game, file)
            shape.x = x
            shape.y = y
